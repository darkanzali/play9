import React from 'react';
import {Provider} from "react-redux";
import {store} from './redux/store';
import AppContainer from "./components/appContainer/appContainer";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
    return (
        <Provider store={store}>
            <AppContainer/>
        </Provider>
    );
}

export default App;
