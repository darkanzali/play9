import * as React from 'react'
import {connect} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faStar} from "@fortawesome/free-regular-svg-icons";
import './requestedStars.scss';

class RequestedStars extends React.Component {

    getStars = () => Array.from(
        {length: this.props.requestedStars},
        (_, index) => <FontAwesomeIcon key={index} icon={faStar}/>
    );

    render() {
        const {round, requestedStars} = this.props;

        return (
            <>
                Round {round} ({requestedStars}): {this.getStars()}
            </>
        )
    }
}

const mapStateToProps = state => {
    return {
        requestedStars: state.requestedStars,
        round: state.round,
    }
};

export default connect(mapStateToProps)(RequestedStars);
