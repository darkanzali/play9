import * as React from 'react';
import GameContainer from "../gameContainer/gameContainer";
import {BrowserRouter as Router, Link, Redirect, Route, Switch} from "react-router-dom";
import TutorialContainer, {TUTORIAL_FIRST_PAGE} from "../tutorialContainer/tutorialContainer";
import {Nav, Navbar} from "react-bootstrap";

class AppContainer extends React.Component {
    render() {
        return (
            <Router>
                <Navbar bg="light" expand="lg">
                    <Navbar.Brand>Play 9</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                            <Link to="/play9" className="nav-link">Play</Link>
                            <Link to={/help/ + TUTORIAL_FIRST_PAGE} className="nav-link">Tutorial</Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>

                <Switch>
                    <Route path="/" exact={true}>
                        <Redirect to="/play9"/>
                    </Route>

                    <Route path="/play9">
                        <GameContainer/>
                    </Route>

                    <Route path="/help/:pageNumber">
                        <TutorialContainer/>
                    </Route>
                </Switch>
            </Router>
        )
    }
}

export default AppContainer;
