import * as React from "react";
import {Button, Modal} from "react-bootstrap";
import {hideModal} from "../../redux/actions";
import {connect} from "react-redux";
import SadCat from '../../assets/sad-cat.gif';

const doHide = (hideModal, onHide) => {
    hideModal();
    onHide();
};

const getCat = (show) => {
    if (show) {
        return (
            <div className="text-center mt-3">
                <img src={SadCat} alt=":("/>
            </div>
        );
    }

    return null;
};

const AppModal = ({modal, timer, hideModal, onHide}) => (
    <Modal show={modal.display} onHide={() => doHide(hideModal, onHide)}>
        <Modal.Header closeButton>
            <Modal.Title>{modal.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <div className="text-center">
                {modal.content}
            </div>
            {getCat(timer === 0)}
        </Modal.Body>
        <Modal.Footer>
            <Button variant="success" onClick={() => doHide(hideModal, onHide)}>
                Restart
            </Button>
        </Modal.Footer>
    </Modal>
);

const mapStateToProps = (state) => {
    return {
        modal: state.modal,
        timer: state.timer.value,
    }
};

const mapDispatchToProps = {
    hideModal,
};

export default connect(mapStateToProps, mapDispatchToProps)(AppModal);
