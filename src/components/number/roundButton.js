import React from "react";
import './roundButton.scss';

const RoundButton = ({onClick, text}) => (
    <button onClick={onClick} className='btn btn-outline-primary btn-round btn-number'>
        {text}
    </button>
);

export default RoundButton;
