import * as React from 'react';
import {connect} from "react-redux";
import {Button, Card, Col, Row} from "react-bootstrap";
import Question from "../requestedStars/requestedStars";
import SelectedNumbers from "../selectedNumbers/selectedNumbers";
import AvailableNumbers from "../availableNumbers/availableNumbers";
import {
    decrementRedraws,
    nextQuestion,
    nextRound,
    resetAvailableNumbers,
    resetRedraws,
    resetRound,
    resetSelectedNumbers,
    resetTimer,
    selectNumber,
    setNumberAsAvailable,
    setNumberAsUnavailable,
    setNumbersAsAvailable,
    setRequestedStars,
    showModal,
} from "../../redux/actions";
import './gameContainer.scss';
import {Utils} from "../../utils/utils";
import AppModal from "../appModal/appModal";
import {Constants} from "../../utils/constants";
import Timer from "../timer/timer";

class GameContainer extends React.Component {

    isAnswerCorrect = () => {
        const {selectedNumbers, requestedStars} = this.props;

        const starsSum = Utils.sumArray(selectedNumbers);

        return requestedStars === starsSum;
    };

    submitAnswer = () => {
        const {nextQuestion, resetSelectedNumbers, availableNumbers, showModal, resetTimer, nextRound} = this.props;

        resetSelectedNumbers();

        if (availableNumbers.length > 0) {
            nextQuestion(availableNumbers);
            resetTimer();
            nextRound();
        } else {
            showModal('You won!', 'Congratulations! You won the game.');
        }
    };

    drawAgain = () => {
        const {
            nextQuestion,
            selectedNumbers,
            availableNumbers,
            resetSelectedNumbers,
            setNumbersAsAvailable,
            decrementRedraws,
            availableRedraws,
        } = this.props;

        if (availableRedraws <= 0) {
            return;
        }

        decrementRedraws();

        const numbers = [...selectedNumbers, ...availableNumbers];

        setNumbersAsAvailable(selectedNumbers);
        resetSelectedNumbers();

        nextQuestion(numbers);
    };

    resetGame = () => {
        const {
            nextQuestion,
            resetAvailableNumbers,
            resetRound,
            resetSelectedNumbers,
            resetTimer,
            resetRedraws,
        } = this.props;

        resetRedraws();
        resetAvailableNumbers();
        resetRound();
        resetSelectedNumbers();
        resetTimer();

        nextQuestion(Utils.rangeArray(Constants.minValue, Constants.maxValue));
    };

    getButtonsRow = () => {
        const {availableRedraws, hideRedraw, hideReset} = this.props;

        return (
            <Row>
                <Col>
                    <Button variant="success" className="mr-2" disabled={!this.isAnswerCorrect()}
                            onClick={this.submitAnswer}>
                        Submit
                    </Button>
                    {
                        hideRedraw ?
                            null :
                            <Button variant="warning" className="mr-2" onClick={this.drawAgain}
                                    disabled={availableRedraws <= 0}>
                                Draw stars again ({availableRedraws})
                            </Button>
                    }
                    {
                        hideReset ?
                            null :
                            <Button variant="danger" onClick={this.resetGame}>
                                Reset game
                            </Button>
                    }
                </Col>
            </Row>
        );
    };

    render() {
        const {hideSelected, hideAvailable, hideTimer, hideButtons, disableUnselect, disableSelect} = this.props;

        return (
            <>
                <div className="app">
                    <div className="app__container">
                        <Row className="mb-4">
                            <Col>
                                <Card>
                                    <Card.Body className="card__content">
                                        <Card.Text>
                                            <Question/>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>

                            <Col>
                                <Card>
                                    <Card.Body className="card__content">
                                        <Card.Text>
                                            {hideSelected ? null : <SelectedNumbers disableUnselect={disableUnselect}/>}
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>

                        <Row className="mb-4">
                            <Col>
                                <Card>
                                    <Card.Body className="card__content">
                                        <Card.Text>
                                            {hideAvailable ? null : <AvailableNumbers disableSelect={disableSelect}/>}
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>

                        <Row className="mb-4">
                            <Col>
                                {hideTimer ? null : <Timer/>}
                            </Col>
                        </Row>

                        {hideButtons ? null : this.getButtonsRow()}
                    </div>
                </div>


                <AppModal onHide={this.resetGame}/>
            </>
        )
    }

    componentDidMount() {
        this.resetGame();

        const {stars, preselectedNumbers, setRequestedStars, setNumberAsUnavailable, selectNumber} = this.props;

        if (stars) {
            setRequestedStars(stars);
        }

        if (preselectedNumbers) {
            preselectedNumbers.forEach(number => {
                setNumberAsUnavailable(number);
                selectNumber(number);
            });
        }
    }
}

const mapStateToProps = (state) => {
    return {
        requestedStars: state.requestedStars,
        selectedNumbers: state.selectedNumbers,
        availableNumbers: state.availableNumbers,
        modal: state.modal,
        availableRedraws: state.redraws,
    }
};

const mapDispatchToProps = {
    nextQuestion,
    setRequestedStars,
    resetRound,
    showModal,
    resetTimer,
    selectNumber,
    setNumberAsAvailable,
    setNumbersAsAvailable,
    setNumberAsUnavailable,
    resetAvailableNumbers,
    resetSelectedNumbers,
    decrementRedraws,
    resetRedraws,
    nextRound,
};

export default connect(mapStateToProps, mapDispatchToProps)(GameContainer);
