import * as React from 'react';
import {useEffect} from 'react';
import {connect} from "react-redux";
import {decrementTimer, showModal} from "../../redux/actions";

const Timer = ({timer, decrementTimer, showModal}) => {
    useEffect(() => {
        if (timer > 0) {
            const timerId = setTimeout(() => decrementTimer(), 1000);

            return () => clearTimeout(timerId);
        } else {
            showModal('Game over!', 'You lost. The time is over! The cat is disappointed.');
        }
    });

    return (
        <>
            Timer: {timer} sec
        </>
    );
};

const mapStateToProps = (state) => {
    return {
        timer: state.timer.value
    }
};

const mapDispatchToProps = {
    decrementTimer,
    showModal,
};

export default connect(mapStateToProps, mapDispatchToProps)(Timer);
