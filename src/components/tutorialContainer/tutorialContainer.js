import * as React from 'react';
import {Button, Col, Row} from "react-bootstrap";
import {Redirect, withRouter} from "react-router";
import StarsTutorial from "./tutorialPages/starsTutorial";
import AvailableNumbersTutorial from "./tutorialPages/availableNumbersTutorial";
import SelectedNumbersTutorial from "./tutorialPages/selectedNumbersTutorial";
import CorrectAnswerTutorial from "./tutorialPages/correctAnswerTutorial";
import RedrawTutorial from "./tutorialPages/redrawTutorial";
import OutOfTimeTutorial from "./tutorialPages/outOfTimeTutorial";

export const TUTORIAL_FIRST_PAGE = 1;
export const TUTORIAL_LAST_PAGE = 6;

class TutorialContainer extends React.Component {
    getPageNumber = () => {
        const {pageNumber} = this.props.match.params;

        return Number(pageNumber);
    };

    setPageNumber = (pageNumber) => {
        const {history} = this.props;
        history.push('/help/' + pageNumber);
    };

    getTutorialStep = (pageNumber) => {
        switch (pageNumber) {
            case 1:
                return (<StarsTutorial/>);
            case 2:
                return (<AvailableNumbersTutorial/>);
            case 3:
                return (<SelectedNumbersTutorial/>);
            case 4:
                return (<CorrectAnswerTutorial/>);
            case 5:
                return (<RedrawTutorial/>);
            case 6:
                return (<OutOfTimeTutorial/>);
            default:
                return (<Redirect to={'/help/' + TUTORIAL_FIRST_PAGE}/>);
        }
    };

    render() {
        const pageNumber = this.getPageNumber();

        return (
            <div className="app">
                <div className="app__container">
                    <Row className="mb-4">
                        <Col>
                            <h2 className="mb-4">How to play?</h2>

                            {this.getTutorialStep(pageNumber)}
                        </Col>
                    </Row>

                    <Row className="mb-4">
                        <Col>
                            <Button
                                disabled={pageNumber === TUTORIAL_FIRST_PAGE}
                                onClick={() => this.setPageNumber(pageNumber - 1)}
                            >
                                Previous
                            </Button>
                        </Col>
                        <Col>
                            <Button className="float-right"
                                    disabled={pageNumber === TUTORIAL_LAST_PAGE}
                                    onClick={() => this.setPageNumber(pageNumber + 1)}
                            >
                                Next
                            </Button>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

export default withRouter(TutorialContainer);
