import * as React from "react";
import GameContainer from "../../gameContainer/gameContainer";

const AvailableNumbersTutorial = () => (
    <>
        <p>Then you select numbers from bottom list, it would appear on list on the right.</p>

        <GameContainer hideTimer={true} hideButtons={true} disableUnselect={true}/>
    </>
);

export default AvailableNumbersTutorial;
