import * as React from "react";
import GameContainer from "../../gameContainer/gameContainer";

const RedrawTutorial = () => (
    <>
        <p>You can redraw number of stars, per instance if you're out of select ideas. You have only 5 redraws in a
            game.</p>

        <GameContainer hideTimer={true} disableSelect={true} hideReset={true}/>
    </>
);

export default RedrawTutorial;
