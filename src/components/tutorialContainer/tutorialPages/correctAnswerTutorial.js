import * as React from "react";
import GameContainer from "../../gameContainer/gameContainer";

const CorrectAnswerTutorial = () => (
    <>
        <p>Once selected numbers would sum up to drawn number of stars confirm button would become active.</p>

        <p>If you would click it, next turn would begin: next number of stars would be drawn and used numbers would
            become unavailable to select.</p>

        <GameContainer hideTimer={true} stars={5} preselectedNumbers={[3, 2]} hideRedraw={true} hideReset={true}/>
    </>
);

export default CorrectAnswerTutorial;
