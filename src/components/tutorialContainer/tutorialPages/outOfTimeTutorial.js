import * as React from "react";
import GameContainer from "../../gameContainer/gameContainer";

const OutOfTimeTutorial = () => (
    <>
        <p>When time counter will reach 0 you will loose and cat will be disappointed</p>

        <GameContainer hideButtons={true} disableSelect={true} disableUnselect={true}/>
    </>
);

export default OutOfTimeTutorial;
