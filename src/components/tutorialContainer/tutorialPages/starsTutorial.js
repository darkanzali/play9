import * as React from "react";
import GameContainer from "../../gameContainer/gameContainer";

const StarsTutorial = () => {
    return (
        <>
            <p>Random number of stars is drawn</p>

            <GameContainer hideTimer={true} hideSelected={true} hideAvailable={true} hideButtons={true}/>
        </>
    );
};

export default StarsTutorial;
