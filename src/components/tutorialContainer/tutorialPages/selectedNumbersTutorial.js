import * as React from "react";
import GameContainer from "../../gameContainer/gameContainer";

const SelectedNumbersTutorial = () => (
    <>
        <p>If you would pick wrong number, you can click it on list on right to remove it.</p>

        <GameContainer hideTimer={true} hideButtons={true} disableSelect={true} stars={5} preselectedNumbers={[9]}/>
    </>
);

export default SelectedNumbersTutorial;
