import * as React from 'react';
import {connect} from "react-redux";
import RoundButton from "../number/roundButton";
import {selectNumber, setNumberAsUnavailable} from "../../redux/actions";

class AvailableNumbers extends React.Component {
    select = (number) => {
        if (this.props.disableSelect) {
            return;
        }

        this.props.selectNumber(number);
        this.props.setNumberAsUnavailable(number);
    };

    getNumbers = () => (
        this.props.numbers.map(number =>
            <RoundButton key={number} text={number} onClick={() => this.select(number)}/>
        )
    );

    render() {
        return (
            <>
                {this.getNumbers()}
            </>
        )
    }
}

const mapStateToProps = state => {
    return {
        numbers: state.availableNumbers
    }
};

const mapDispatchToProps = {
    selectNumber,
    setNumberAsUnavailable
};

export default connect(mapStateToProps, mapDispatchToProps)(AvailableNumbers);
