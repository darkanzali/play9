import * as React from 'react';
import {connect} from "react-redux";
import RoundButton from "../number/roundButton";
import {setNumberAsAvailable, unselectNumber} from "../../redux/actions";

class SelectedNumbers extends React.Component {
    unselect = (number) => {
        if (this.props.disableUnselect) {
            return;
        }

        this.props.unselectNumber(number);
        this.props.setNumberAsAvailable(number);
    };

    getNumbers = () => (
        this.props.numbers.map(number =>
            <RoundButton key={number} text={number} onClick={() => this.unselect(number)}/>
        )
    );

    render() {
        return (
            <>
                Selected:
                {this.getNumbers()}
            </>
        )
    }
}

const mapStateToProps = state => {
    return {
        numbers: state.selectedNumbers
    }
};

const mapDispatchToProps = {
    unselectNumber,
    setNumberAsAvailable
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectedNumbers);
