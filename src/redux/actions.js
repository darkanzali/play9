import ActionTypes from './actionTypes';

export const nextQuestion = (availableNumbers) => ({
    type: ActionTypes.nextQuestion,
    availableNumbers,
});

export const setRequestedStars = (stars) => ({
    type: ActionTypes.setRequestedStars,
    stars: stars,
});

export const resetSelectedNumbers = () => ({
    type: ActionTypes.resetSelectedNumbers,
});

export const selectNumber = (number) => ({
    type: ActionTypes.selectNumber,
    number: number,
});

export const unselectNumber = (number) => ({
    type: ActionTypes.unselectNumber,
    number: number,
});

export const resetAvailableNumbers = () => ({
    type: ActionTypes.resetAvailableNumbers,
});

export const setNumberAsUnavailable = (number) => ({
    type: ActionTypes.setNumberAsUnavailable,
    number: number,
});

export const setNumbersAsAvailable = (numbers) => ({
    type: ActionTypes.setNumbersAsAvailable,
    numbers: numbers,
});

export const setNumberAsAvailable = (number) => ({
    type: ActionTypes.setNumberAsAvailable,
    number: number,
});

export const nextRound = () => ({
    type: ActionTypes.nextRound,
});

export const resetRound = () => ({
    type: ActionTypes.resetRound,
});

export const showModal = (title = '', content = '') => ({
    type: ActionTypes.showModal,
    title,
    content,
});

export const hideModal = () => ({
    type: ActionTypes.hideModal,
});

export const decrementTimer = () => ({
    type: ActionTypes.decrementTimer,
});

export const resetTimer = () => ({
    type: ActionTypes.resetTimer,
});

export const decrementRedraws = () => ({
    type: ActionTypes.decrementRedraws,
});

export const resetRedraws = () => ({
    type: ActionTypes.resetRedraws,
});
