import ActionTypes from '../actionTypes'

export const ModalReducer = (state = {display: false, title: false, content: false}, action) => {
    switch (action.type) {
        case ActionTypes.showModal:
            return {display: true, title: action.title, content: action.content};
        case ActionTypes.hideModal:
            return {display: false, title: '', content: ''};
        default:
            return state;
    }
};
