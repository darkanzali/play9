import ActionTypes from '../actionTypes'

export const SelectedNumbersReducer = (state = [], action) => {
    switch (action.type) {
        case ActionTypes.selectNumber:
            return [...state, action.number].sort();
        case ActionTypes.unselectNumber:
            state.splice(state.indexOf(action.number), 1);
            return [...state];
        case ActionTypes.resetSelectedNumbers:
            return [];
        default:
            return state;
    }
};
