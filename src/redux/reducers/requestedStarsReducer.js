import ActionTypes from '../actionTypes';
import {Utils} from '../../utils/utils'
import {Constants} from "../../utils/constants";

const MIN_NUMBER = Constants.minValue;
const MAX_NUMBER = Constants.maxValue;

export const RequestedStarsReducer = (state = Utils.randomSumIn(Utils.rangeArray(MIN_NUMBER, MAX_NUMBER), MAX_NUMBER), action) => {
    switch (action.type) {
        case ActionTypes.nextQuestion:
            return Utils.randomSumIn(action.availableNumbers, MAX_NUMBER);
        case ActionTypes.setRequestedStars:
            return action.stars;
        default:
            return state;
    }
};
