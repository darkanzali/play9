import {combineReducers} from "redux";
import {SelectedNumbersReducer} from "./selectedNumbersReducer";
import {RequestedStarsReducer} from "./requestedStarsReducer";
import {AvailableNumbersReducer} from "./availableNumbersReducer";
import {RoundReducer} from "./roundReducer";
import {ModalReducer} from "./modalReducer";
import {TimerReducer} from "./timerReducer";
import {RedrawsReducer} from "./redrawsReducer";

export default combineReducers({
    selectedNumbers: SelectedNumbersReducer,
    availableNumbers: AvailableNumbersReducer,
    requestedStars: RequestedStarsReducer,
    round: RoundReducer,
    modal: ModalReducer,
    timer: TimerReducer,
    redraws: RedrawsReducer,
});
