import ActionTypes from '../actionTypes';

const FIRST_ROUND_NUMBER = 1;

export const RoundReducer = (state = FIRST_ROUND_NUMBER, action) => {
    switch (action.type) {
        case ActionTypes.nextRound:
            return state + 1;
        case ActionTypes.resetRound:
            return FIRST_ROUND_NUMBER;
        default:
            return state;
    }
};
