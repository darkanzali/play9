import ActionTypes from '../actionTypes'
import {Constants} from "../../utils/constants";

export const RedrawsReducer = (state = Constants.availableRedraws, action) => {
    switch (action.type) {
        case ActionTypes.decrementRedraws:
            if (state >= 1) {
                state--;
            }

            return state;
        case ActionTypes.resetRedraws:
            return Constants.availableRedraws;
        default:
            return state;
    }
};
