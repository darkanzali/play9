import ActionTypes from '../actionTypes'
import {Constants} from "../../utils/constants";

const initialState = () => {
    return {paused: false, value: Constants.timerStartValue};
};

export const TimerReducer = (state = initialState(), action) => {
    switch (action.type) {
        case ActionTypes.decrementTimer:
            if (state.value >= 1 && !state.paused) {
                state.value--;
            }

            return {paused: state.paused, value: state.value};
        case ActionTypes.resetTimer:
            return initialState();
        default:
            return state;
    }
};
