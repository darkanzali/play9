import ActionTypes from '../actionTypes'
import {Utils} from '../../utils/utils'
import {Constants} from "../../utils/constants";

const initialNumbers = () => Utils.rangeArray(Constants.minValue, Constants.maxValue);

export const AvailableNumbersReducer = (state = initialNumbers(), action) => {
    switch (action.type) {
        case ActionTypes.setNumberAsAvailable:
            return [...state, action.number].sort();
        case ActionTypes.setNumbersAsAvailable:
            return [...state, ...action.numbers].sort();
        case ActionTypes.setNumberAsUnavailable:
            state.splice(state.indexOf(action.number), 1);
            return [...state];
        case ActionTypes.resetAvailableNumbers:
            return initialNumbers();
        default:
            return state;
    }
};
