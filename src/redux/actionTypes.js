export default {
    // SelectedNumbersReducer
    selectNumber: 'SELECT_NUMBER',
    unselectNumber: 'UNSELECT_NUMBER',
    resetSelectedNumbers: 'RESET_SELECTED_NUMBERS',

    // AvailableNumbersReducer
    setNumbersAsAvailable: 'SET_NUMBERS_AS_AVAILABLE',
    setNumberAsAvailable: 'SET_NUMBER_AS_AVAILABLE',
    setNumberAsUnavailable: 'SET_NUMBER_AS_UNAVAILABLE',
    resetAvailableNumbers: 'RESET_AVAILABLE_NUMBERS',

    // RequestedStarsReducer
    nextQuestion: 'NEXT_QUESTION',
    setRequestedStars: 'SET_REQUESTED_STARS',

    // RoundReducer
    nextRound: 'NEXT_ROUND',
    resetRound: 'RESET_ROUND',

    // ModalReducer
    showModal: 'SHOW_MODAL',
    hideModal: 'HIDE_MODAL',

    // TimerReducer
    decrementTimer: 'DECREMENT_TIMER',
    resetTimer: 'RESET_TIMER',

    // HintsReducer
    decrementRedraws: 'DECREMENT_REDRAWS',
    resetRedraws: 'RESET_REDRAWS',

    // TutorialStepReducer
    tutorialNextPage: 'TUTORIAL_NEXT_PAGE',
    tutorialPreviousPage: 'TUTORIAL_PREVIOUS_PAGE',
}
