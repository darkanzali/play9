export const Utils = {
    sumArray: arr => arr.reduce((accumulator, currentValue) => accumulator + currentValue, 0),

    rangeArray: (lowerBound, upperBound) => Array.from({length: upperBound - lowerBound + 1}, (_, i) => lowerBound + i),

    randomNumber: (minValue, maxValue) => minValue + Math.floor(maxValue * Math.random()),

    randomSumIn: (array, maxValue) => {
        const sets = [[]];
        const possibleAnswers = [];

        for (let arrayIndex = 0; arrayIndex < array.length; arrayIndex++) {
            for (let setsIndex = 0; setsIndex < sets.length; setsIndex++) {
                if (sets[setsIndex].indexOf(array[arrayIndex]) === -1) {
                    const candidateSet = sets[setsIndex].concat(array[arrayIndex]);
                    const candidateSum = Utils.sumArray(candidateSet);

                    if (candidateSum <= maxValue) {
                        sets.push(candidateSet);
                        possibleAnswers.push({set: candidateSet, sum: candidateSum});
                    }
                }
            }
        }

        let rounds = 10;
        let candidateAnswer;

        do {
            candidateAnswer = possibleAnswers[Utils.randomNumber(0, possibleAnswers.length)];
            rounds--;
        } while (rounds > 0 && candidateAnswer.set.length === 1);

        return candidateAnswer.sum;
    }
};
