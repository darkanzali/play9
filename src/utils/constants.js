export const Constants = {
    minValue: 1,
    maxValue: 9,
    timerStartValue: 9,
    availableRedraws: 5,
};
